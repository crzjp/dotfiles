#!/usr/bin/env sh

[ "$1" = "-x" ] && set -x

for dep in mpd mpc notify-send ; do
    [ $(command -v $dep) ] || { echo error: \"$dep\" not found && exit 1; }
done

if ! pgrep -x mpd >/dev/null ; then
    notify-send -h string:x-canonical-private-synchronous:mpd \
        "Error" "mpd is not running!"
    exit 1
fi

title="$(mpc current -f %title%)"
artist="$(mpc current -f %artist%)"
album="$(mpc current -f %album%)"
file="$HOME/musics/$(mpc current -f %file%)"
coverdir="$HOME/musics/covers"
cover="$coverdir/$artist-$album.png"

[ ! -d "$coverdir" ] && mkdir "$coverdir"
[ ! -f "$cover" ] && ffmpeg -nostats -loglevel 0 -y -i "$file" "$cover"

notify-send -u low -i "$cover" \
    -h string:x-canonical-private-synchronous:mpd \
    "$title" "$artist\n$album"
