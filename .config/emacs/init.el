;; -*- lexical-binding: t; -*-

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(setopt package-user-dir (expand-file-name "elpa" user-emacs-directory)
        package-gnupghome-dir (expand-file-name "gnupg" package-user-dir))

(package-initialize)

(setopt use-package-always-ensure t
        use-package-always-defer t
        use-package-expand-minimally t)

(setopt user-full-name "João Paulo da Cruz"
        user-mail-address "joaopcruz15@gmail.com")

(keymap-global-set "M-&" 'with-editor-async-shell-command)

(setopt global-auto-revert-non-file-buffers t)

(global-auto-revert-mode 1)

(cd "~/")

(setopt delete-by-moving-to-trash t
        trash-directory (expand-file-name "emacs-trash" temporary-file-directory))

(delete-selection-mode 1)

(setopt kill-do-not-save-duplicates t)

(setopt disabled-command-function nil)

(setopt auto-save-list-file-prefix (expand-file-name "autosaves/" user-emacs-directory)
        auto-save-file-name-transforms `((".*" ,(expand-file-name "autosaves/" user-emacs-directory) t)))

(setopt backup-directory-alist `((".*" . ,(expand-file-name "backups" user-emacs-directory)))
        backup-by-copying t
        version-control t
        delete-old-versions t
        vc-make-backup-files t
        kept-old-versions 10
        kept-new-versions 10)

(setopt custom-file (expand-file-name "custom.el" user-emacs-directory))

(when (file-exists-p custom-file)
  (load custom-file t))

(use-package recentf
  :ensure nil
  :defer 2
  :bind ("C-c r" . recentf-open)
  :custom
  (recentf-max-saved-items 100)
  (recentf-auto-cleanup (* 5 60))
  (recentf-exclude
   '("\\.[jp][pn]g\\'" "\\.webp\\'" "\\.pdf\\'" "\\.gpg\\'" "\\.zip\\'"
     "/gnu/.*" "\\.cache/.*" "\\.local/share/.*" ".*/mail/.*"
     "/var/.*" "~/.guix-profile/.*" "~/.guix-home/.*"))
  :config
  (recentf-mode 1))

(setopt vc-follow-symlinks t)

(setopt help-window-select t)

(setopt Man-notify-method 'pushy
        woman-fill-frame t)

(setenv "PAGER" "cat")
(setenv "MANPAGER" "cat")

(setopt use-short-answers t)

(setopt large-file-warning-threshold nil)

(setopt indent-tabs-mode nil
        tab-width 4)

(keymap-global-unset "C-z")
(keymap-global-unset "C-x C-z")

(setopt sentence-end-double-space nil)

(setopt uniquify-buffer-name-style 'forward)

(setopt bookmark-save-flag 1)

(setopt auth-sources '("~/.authinfo.gpg"))

(setopt isearch-lazy-count t)

(pixel-scroll-precision-mode 1)

(setopt mode-line-end-spaces nil)

(set-display-table-slot standard-display-table 'vertical-border (make-glyph-code ?│))

(xterm-mouse-mode 1)

(keymap-global-set "C-c u" 'browse-url-at-point)

(setopt confirm-kill-emacs 'yes-or-no-p)

(setopt fill-column 80)

(use-package cape
  :defer 1
  :config
  (add-hook 'completion-at-point-functions 'cape-dabbrev)
  (add-hook 'completion-at-point-functions 'cape-file))

(use-package corfu
  :defer 1
  :bind (:map corfu-map
         ("RET" . corfu-send)
         ("M-m" . (lambda ()
                    (interactive)
                    (let ((completion-extra-properties corfu--extra)
                          completion-cycle-threshold completion-cycling)
                      (apply #'consult-completion-in-region completion-in-region--data)))))
  :custom
  (corfu-preview-current nil)
  :config
  (global-corfu-mode 1))

(use-package vertico
  :defer 1
  :bind (:map vertico-map
         ("DEL" . vertico-directory-delete-char))
  :config
  (vertico-mode 1))

(setopt completion-auto-help 'visible
        completion-auto-select 'second-tab
        completion-show-help nil
        completions-format 'one-column
        completions-max-height 12)

(add-hook 'completion-list-mode-hook
          #'(lambda () (setq-local truncate-lines t)))

(use-package consult
  :after vertico
  :demand nil
  :bind (("C-c c SPC" . consult-mark)
         ("C-c c o" . consult-outline)
         ("C-c c i" . consult-info)
         ("C-c c f" . consult-find)
         ("C-c c g" . consult-grep)
         ("C-c c s" . consult-search)
         :map minibuffer-mode-map
         ("C-r" . consult-history))
  :custom
  (completion-in-region-function
   (lambda (&rest args)
     (apply (if (or vertico-mode fido-vertical-mode)
                'consult-completion-in-region
              'completion--in-region)
            args))))

(use-package marginalia
  :after vertico
  :demand nil
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :config
  (marginalia-mode 1))

(setopt read-extended-command-predicate 'command-completion-default-include-p)

(setopt history-delete-duplicates t)

(savehist-mode 1)

(setopt enable-recursive-minibuffers t)

(minibuffer-depth-indicate-mode 1)

(setopt minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))

(add-hook 'minibuffer-setup-hook 'cursor-intangible-mode)

(use-package orderless
  :after vertico
  :custom
  (read-buffer-completion-ignore-case t)
  (read-file-name-completion-ignore-case t)
  (completion-ignore-case t)
  (completion-styles '(basic substring initials flex orderless))
  (completion-category-overrides '((file (styles . (basic partial-completion orderless)))))
  (completion-category-defaults nil)
  (orderless-matching-styles '(orderless-prefixes orderless-regexp)))

(use-package dired
  :ensure nil
  :bind (("C-x C-d" . dired-jump)
         :map dired-mode-map
         ("b" . dired-up-directory))
  :hook (dired-mode . (lambda () (setq truncate-lines t)))
  :custom
  (dired-listing-switches "-agGh --group-directories-first")
  (dired-kill-when-opening-new-dired-buffer t)
  (dired-dwim-target t))

(use-package diredfl
  :hook dired-mode)

(use-package ibuffer
  :ensure nil
  :bind (([remap list-buffers] . ibuffer)
         ("C-c b" . ibuffer))
  :hook (ibuffer-mode . ibuffer-auto-mode)
  :custom
  (ibuffer-formats
   '((mark modified read-only locked " "
           (name 20 20 :left :elide)
           " "
           (size-h 11 -1 :right)
           " "
           (mode 16 16 :left :elide)
           " " filename-and-process)
     (mark " "
           (name 16 -1)
           " " filename))))

(use-package ibuffer
  :ensure nil
  :hook (ibuffer-mode . (lambda () (ibuffer-switch-to-saved-filter-groups "Default")))
  :custom
  (ibuffer-saved-filter-groups
   '(("Default"
      ("Modified" (and (modified . t)
                       (visiting-file . t)))
      ("Term" (or (mode . eat-mode)
                  (mode . eshell-mode)
                  (mode . term-mode)
                  (mode . shell-mode)))
      ("Debug" (mode . debugger-mode))
      ("Agenda" (filename . "agenda.org"))
      ("Org" (mode . org-mode))
      ("VC" (or (name . "^magit.*")
                (name . "^\*vc.*")))
      ("Mail" (name . "^\*mu4e.*"))
      ("Book" (or (mode . pdf-view-mode)
                  (mode . nov-mode)))
      ("Dired" (mode . dired-mode))
      ("Chat" (mode . erc-mode))
      ("Help" (or (mode . help-mode)
                  (mode . Info-mode)
                  (mode . woman-mode)
                  (mode . Man-mode)
                  (mode . Custom-mode)
                  (mode . apropos-mode)))
      ("Media" (or (mode . image-mode)
                   (name . "^\*Mingus.*")
                   (name . "^\*transmission.*")
                   (mode . gomoku-mode)))
      ("Internal" (name . "^\*.*$"))
      ("Misc" (name . "^.*$")))))
  (ibuffer-show-empty-filter-groups nil))

(use-package ibuffer
  :ensure nil
  :config  
  (defun crz/human-readable-file-sizes-to-bytes (string)
    "Convert a human-readable file size into bytes."
    (cond
     ((string-suffix-p "G" string t)
      (* 1000000000 (string-to-number (substring string 0 (- (length string) 1)))))
     ((string-suffix-p "M" string t)
      (* 1000000 (string-to-number (substring string 0 (- (length string) 1)))))
     ((string-suffix-p "K" string t)
      (* 1000 (string-to-number (substring string 0 (- (length string) 1)))))
     (t
      (string-to-number (substring string 0 (- (length string) 1))))))
  (defun crz/bytes-to-human-readable-file-sizes (bytes)
    "Convert number of bytes to human-readable file size."
    (cond
     ((> bytes 1000000000) (format "%10.1fG" (/ bytes 1000000000.0)))
     ((> bytes 100000000) (format "%10.0fM" (/ bytes 1000000.0)))
     ((> bytes 1000000) (format "%10.1fM" (/ bytes 1000000.0)))
     ((> bytes 100000) (format "%10.0fK" (/ bytes 1000.0)))
     ((> bytes 1000) (format "%10.1fK" (/ bytes 1000.0)))
     (t (format "%10d" bytes))))
  (define-ibuffer-column size-h
    (:name "Size"
           :inline t
           :summarizer
           (lambda (column-strings)
             (let ((total 0))
               (dolist (string column-strings)
                 (setq total
                       (+ (float (crz/human-readable-file-sizes-to-bytes string))
                          total)))
               (crz/bytes-to-human-readable-file-sizes total))))
    (crz/bytes-to-human-readable-file-sizes (buffer-size))))

(use-package cider
  :hook (clojure-mode . cider-mode)
  :config
  (add-to-list 'completion-category-defaults '(cider (styles basic))))

(use-package sly
  :custom
  (inferior-lisp-program "sbcl"))

(use-package sly-mrepl
  :ensure nil
  :after sly
  :bind (:map sly-mrepl-mode-map
         ("C-r" . consult-history))
  :custom
  (sly-mrepl-history-file-name (expand-file-name "sly-mrepl-history" user-emacs-directory)))

(setopt eldoc-echo-area-use-multiline-p nil)

(use-package markdown-mode
  :mode (("\\.md\\'" . markdown-mode)
         ("README\\.md\\'" . gfm-mode)))

(use-package rust-mode)

(use-package magit
  :bind ("C-c g" . magit-status)
  :custom
  (magit-display-buffer-function 'magit-display-buffer-fullframe-status-v1)
  (magit-bury-buffer-function 'magit-restore-window-configuration))

(use-package pdf-tools
  :ensure nil
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :hook (pdf-view-mode . pdf-view-themed-minor-mode))

(use-package nov
  :mode ("\\.epub\\'" . nov-mode))

(use-package gnus
  :ensure nil
  :bind ("C-c m" . gnus)
  :custom
  (send-mail-function 'smtpmail-send-it)
  (smtpmail-smtp-server "smtp.gmail.com")
  (smtpmail-smtp-service 587)
  (message-directory "~/public/mail")
  (mail-source-directory message-directory)
  (mail-user-agent 'gnus-user-agent)
  (read-mail-command 'gnus)
  (gnus-home-directory (expand-file-name "gnus" user-emacs-directory))
  (gnus-directory (expand-file-name "news" gnus-home-directory))
  (gnus-select-method '(nnimap "gmail"
                               (nnimap-address "imap.gmail.com")
                               (nnimap-server-port 993)
                               (nnimap-stream ssl)
                               (nnimap-authinfo-file "~/.authinfo.gpg"))))

(use-package erc
  :ensure nil
  :custom
  (erc-accidental-paste-threshold-seconds nil)
  (erc-nick "crzjp")
  (erc-fill-column (- (window-width) 1))
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 20)
  (erc-prompt (lambda () (concat "[" (buffer-name) "]")))
  (erc-auto-query 'bury)
  (erc-rename-buffers t)
  (erc-autojoin-timing 'ident)
  (erc-autojoin-delay 1)
  (erc-autojoin-channels-alist
   '(("libera.chat" "#emacs" "#guix" "#nonguix" "#nixers" "#stumpwm")
     ("slackjeff.com.br" "#mundo-libre")))
  (erc-track-exclude-types
   '("JOIN" "MODE" "NICK" "PART" "QUIT" "324" "329" "332" "333" "353" "477"))
  (erc-prompt-for-password nil)
  (erc-use-auth-source-for-nickserv-password t)
  :config
  (add-to-list 'erc-modules 'autojoin)
  (add-to-list 'erc-modules 'notifications))

(use-package erc-hl-nicks
  :after erc
  :demand nil
  :config
  (add-to-list 'erc-modules 'hl-nicks))

(use-package mingus
  :custom
  (mingus-use-mouse-p nil)
  (mingus-mode-line-show-elapsed-time nil)
  (mingus-mode-line-show-volume nil))

(use-package transmission
  :custom
  (transmission-refresh-modes
   '(transmission-mode
     transmission-files-mode
     transmission-info-mode
     transmission-peers-mode)))

(use-package org
  :ensure nil
  :mode ("\\.org$" . org-mode)
  :custom
  (org-directory "~/documents/org")
  (org-special-ctrl-a/e t)
  :config
  (add-to-list 'org-export-backends 'md)
  (unless (file-directory-p org-directory)
    (make-directory org-directory t)))

(use-package org
  :ensure nil
  :bind ("C-c a" . org-agenda)
  :custom
  (org-agenda-start-with-log-mode t)
  (org-log-done 'time)
  (org-log-into-drawer t)
  (org-agenda-files '("~/documents/agenda.org")))

(use-package org
  :ensure nil
  :custom
  (org-src-window-setup 'current-window)
  (org-src-preserve-indentation t)
  (org-edit-src-content-indentation 0)
  :config
  (add-to-list 'org-modules 'org-tempo)
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("li" . "src lisp"))
  (add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
  (add-to-list 'org-structure-template-alist '("sh" . "src sh")))

(use-package org
  :ensure nil
  :hook (org-mode . visual-line-mode)
  :custom
  (org-startup-indented t)
  (org-startup-with-inline-images t)
  (org-image-actual-width '(600))
  (org-startup-folded t))

(use-package org-modern
  :after org
  :demand nil
  :config
  (defun crz/org-pretty-mode ()
    (interactive nil org-mode)
    (if org-modern-mode
        (progn
          (setq org-hide-emphasis-markers nil
                org-ellipsis nil)
          (org-mode-restart))
      (setq org-hide-emphasis-markers t
            org-ellipsis " ")
      (org-mode-restart)
      (org-modern-mode 1)
      (variable-pitch-mode 1))))

(use-package proced
  :ensure nil
  :bind ("C-c p" . proced)
  :custom
  (proced-auto-update-flag t)
  (proced-auto-update-interval 2)
  (proced-enable-color-flag t))

(use-package esh-mode
  :ensure nil
  :bind (("C-c e" . eshell)
         :map eshell-mode-map
         ("C-l" . (lambda ()
                    (interactive)
                    (let ((input (eshell-get-old-input)))
                      (eshell/clear t)
                      (eshell-emit-prompt)
                      (insert input)))))
  :custom
  (eshell-buffer-maximum-lines 1000)
  (eshell-scroll-to-bottom-on-input t)
  (eshell-destroy-buffer-when-process-dies t)
  :config
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer))

(use-package em-alias
  :ensure nil
  :custom
  (eshell-aliases-file
   (expand-file-name "eshell-aliases" (file-name-directory user-init-file)))
  :config
  (eshell-read-aliases-list))

(use-package em-cmpl
  :ensure nil
  :custom
  (eshell-cmpl-ignore-case t))

(use-package esh-mode
  :ensure nil
  :bind (:map eshell-mode-map
         ("C-r" . cape-history))
  :hook (eshell-pre-command . eshell-save-some-history)
  :custom
  (eshell-history-size 1000)
  (eshell-hist-ignoredups t))

(use-package em-prompt
  :ensure nil
  :hook (eshell-mode . (lambda () (setq-local outline-regexp eshell-prompt-regexp)))
  :custom
  (eshell-prompt-regexp "^[^$\n]*\\\$ ")
  (eshell-prompt-function (lambda ()
                            (concat
                             (abbreviate-file-name (eshell/pwd)) " λ"
                             (propertize "$" 'invisible t) " "))))

(use-package shell
  :ensure nil
  :bind (("C-c s" . shell)
         :map shell-mode-map
         ("C-r" . cape-history))
  :custom
  (comint-prompt-read-only t)
  (shell-kill-buffer-on-exit t))

(use-package eat
  :bind (("C-c t" . eat)
         ("C-x p t" . eat-project))
  :hook (eshell-load . eat-eshell-visual-command-mode)
  :custom
  (eat-enable-shell-prompt-annotation nil))

(use-package rainbow-mode)

(defun crz/display-ansi-colors ()
  (interactive)
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))

(setopt cursor-type 'bar
        cursor-in-non-selected-windows nil)

(setopt use-dialog-box nil)

(defun crz/set-font-faces ()
  (set-face-attribute 'default nil :font "Iosevka Comfy" :height 105)
  (set-face-attribute 'fixed-pitch nil :font "Iosevka Comfy" :height 105)
  (set-face-attribute 'variable-pitch nil :font "Iosevka Comfy Duo" :height 105))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame (crz/set-font-faces))))
  (crz/set-font-faces))

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(setopt frame-resize-pixelwise t)

(setopt mode-line-compact 'long)

(use-package time
  :ensure nil
  :defer 2
  :custom
  (display-time-default-load-average nil)
  (display-time-24hr-format t)
  :config
  (display-time-mode 1))

(column-number-mode 1)

(use-package tab-bar
  :ensure nil
  :bind (("C-<tab>" . tab-recent)
         ("C-x t b" . tab-switch))
  :custom
  (tab-bar-show 1))

(use-package modus-themes
  :ensure nil
  :bind ("<f5>" . modus-themes-toggle)
  :custom
  (modus-themes-subtle-line-numbers t)
  (modus-themes-org-blocks 'gray-background)
  (modus-themes-mode-line '(borderless accented))
  (modus-themes-fringes nil)
  (modus-themes-region '(bg-only no-extend))
  (modus-themes-prompts '(bold))
  :init
  (load-theme 'modus-vivendi t))

(use-package ace-window
  :bind ("M-o" . ace-window)
  :custom
  (aw-scope 'frame)
  (aw-ignore-current t))

(setopt ediff-keep-variants nil
        ediff-split-window-function 'split-window-horizontally
        ediff-window-setup-function 'ediff-setup-windows-plain)

(use-package pinentry
  :defer 2
  :custom
  (epg-pinentry-mode 'loopback)
  :config
  (pinentry-start))

(use-package envrc
  :hook (after-init . envrc-global-mode)
  :custom
  (envrc-lighter nil))

(use-package 0x0
  :bind ("C-c 0" . 0x0-dwim))
