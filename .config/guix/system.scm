(use-modules
 (gnu)
 (gnu packages base)
 (gnu packages curl)
 (gnu packages disk)
 (gnu packages file)
 (gnu packages file-systems)
 (gnu packages freedesktop)
 (gnu packages glib)
 (gnu packages gnupg)
 (gnu packages linux)
 (gnu packages version-control)
 (gnu packages wm)
 (gnu services base)
 (gnu services desktop)
 (gnu services networking)
 (gnu services ssh)
 (gnu services xorg)
 (nongnu packages linux))

(operating-system
 (kernel linux)
 (kernel-arguments
  (cons* "resume=/dev/sda2"
         %default-kernel-arguments))
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (timezone "America/Bahia")
 (keyboard-layout (keyboard-layout "br" "abnt2"))
 (host-name "ofanim")

 (users (cons* (user-account
                (name "crzjp")
                (comment "João Paulo da Cruz")
                (group "users")
                (home-directory "/home/crzjp")
                (supplementary-groups '("wheel" "netdev" "audio" "video" "input" "tty")))
               %base-user-accounts))

 (packages (cons* binutils
                  curl
                  dbus
                  dosfstools
                  exfat-utils
                  exfatprogs
                  file
                  fuse-exfat
                  git
                  gnupg
                  libinput
                  %base-packages))

 (services
  (cons* (service elogind-service-type)
         (service ntp-service-type)
         (service network-manager-service-type)
         (service wpa-supplicant-service-type)
         (service screen-locker-service-type
                  (screen-locker-configuration
                   (name "swaylock")
                   (program (file-append swaylock "/bin/swaylock"))
                   (using-pam? #t)
                   (using-setuid? #f)))
         (modify-services %base-services
                          (guix-service-type
                           config =>
                           (guix-configuration
                            (inherit config)
                            (substitute-urls
                             (cons* "https://substitutes.nonguix.org"
                                    "https://ci.guix.trop.in"
                                    %default-substitute-urls))
                            (authorized-keys
                             (cons* (plain-file "nonguix.pub"
                                                "(public-key
                                                 (ecc
                                                  (curve Ed25519)
                                                  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))")
                                    %default-authorized-guix-keys)))))))

 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))
 (swap-devices (list (swap-space
                      (target (file-system-label "SWAP")))))
 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device (file-system-label "BOOT"))
                       (type "vfat"))
                      (file-system
                       (mount-point "/")
                       (device (file-system-label "ROOT"))
                       (type "ext4"))
                      %base-file-systems)))
