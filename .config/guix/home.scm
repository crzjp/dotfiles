(use-modules
 (gnu)
 (gnu packages admin)
 (gnu packages bittorrent)
 (gnu packages compression)
 (gnu packages emacs)
 (gnu packages emacs-xyz)
 (gnu packages fonts)
 (gnu packages fontutils)
 (gnu packages freedesktop)
 (gnu packages gimp)
 (gnu packages gnome)
 (gnu packages gnome-xyz)
 (gnu packages gnupg)
 (gnu packages haskell-apps)
 (gnu packages imagemagick)
 (gnu packages libreoffice)
 (gnu packages librewolf)
 (gnu packages linux)
 (gnu packages lisp)
 (gnu packages mpd)
 (gnu packages package-management)
 (gnu packages rsync)
 (gnu packages ssh)
 (gnu packages shellutils)
 (gnu packages terminals)
 (gnu packages video)
 (gnu packages wm)
 (gnu packages xdisorg)
 (gnu home services)
 (gnu home services desktop)
 (gnu home services pm)
 (gnu home services shells)
 (gnu home services sound)
 (gnu home services xdg)
 (nongnu packages compression)
 (radix home services gnupg))

(home-environment
 (packages (cons* adwaita-icon-theme
                  brightnessctl
                  direnv
                  dunst
                  emacs-pdf-tools
                  emacs-pgtk
                  ffmpeg
                  font-awesome
                  font-google-noto
                  font-google-noto-emoji
                  font-google-noto-sans-cjk
                  font-google-noto-serif-cjk
                  font-iosevka-comfy
                  fontconfig
                  foot
                  fuzzel
                  gimp
                  grimshot
                  hicolor-icon-theme
                  imagemagick
                  libnotify
                  libreoffice
                  librewolf
                  materia-theme
                  mpd
                  mpd-mpc
                  mpdscribble
                  mpv
                  openssh
                  rsync
                  sbcl
                  shellcheck
                  stow
                  sway
                  swayidle
                  swaylock
                  transmission
                  tree
                  unrar
                  unzip
                  wireplumber
                  xdg-utils
                  xeyes
                  yt-dlp
                  (specifications->packages
                   (list "steam" "zip"))))

 (services
  (cons* (service home-bash-service-type
                  (home-bash-configuration
                   (guix-defaults? #f)
                   (environment-variables
                    `(("QT_QPA_PLATFORM" . "wayland;xcb")
                      ("QT_WAYLAND_DISABLE_WINDOWDECORATION" . "1")
                      ("LESSHISTFILE" . "$XDG_CACHE_HOME/lesshst")
                      ("GUILE_HISTORY" . "$XDG_CACHE_HOME/guile_history")
                      ("GTK2_RC_FILES" . "$HOME/.config/gtk-2.0/settings.ini")
                      ("PATH" . "$HOME/.local/bin:$PATH")
                      ("LESS" . ,(literal-string "-FRJMWX"))
                      ("PAGER" . "less")
                      ("MANPAGER" . "less")
                      ("VISUAL" . "emacsclient")
                      ("EDITOR" . "emacsclient")
                      ("BROWSER" . "librewolf")
                      ("GPG_TTY" . "$(tty)")))
                   (bash-profile
                    (list (plain-file "profile"
                                      (string-append
                                       "\n"
                                       "eval \"$(dircolors)\""
                                       "\n\n"
                                       "export LESS_TERMCAP_mb=$'\\033[1;31m'\n"
                                       "export LESS_TERMCAP_md=$'\\033[1;36m'\n"
                                       "export LESS_TERMCAP_me=$'\\033[0m'\n"
                                       "export LESS_TERMCAP_so=$'\\033[01;7;34m'\n"
                                       "export LESS_TERMCAP_se=$'\\033[0m'\n"
                                       "export LESS_TERMCAP_us=$'\\033[1;32m'\n"
                                       "export LESS_TERMCAP_ue=$'\\033[0m'"))))
                   (aliases `(("gi" . "guix install")
                              ("gr" . "guix remove")
                              ("gs" . "guix search")
                              ("reboot" . "sudo reboot")
                              ("poweroff" . "sudo shutdown")
                              ("ls" . "ls -AC --color=auto --group-directories-first")
                              ("ll" . "ls -AgGh --color=auto --group-directories-first")
                              ("lt" . "tree -C")
                              ("cp" . "cp -rv")
                              ("mv" . "mv -v")
                              ("rm" . "rm -rfvI")
                              ("mkdir" . "mkdir -pv")
                              ("grep" . "grep --color=always")
                              ("egrep" . "egrep --color=always")
                              ("fgrep" . "fgrep --color=always")
                              ("zgrep" . "zgrep --color=always")
                              ("e" . "emacsclient -nwa ''")
                              ("ed" . "emacs --daemon")
                              ("edk" . "emacsclient -e '(kill-emacs)'")
                              ("mg" . "mg -n")
                              ("ping" . "ping -c 3 gnu.org")
                              ("wttr" . "curl -s wttr.in")
                              ("qttr" . "curl -s wttr.in/?0Q")
                              ("which" . "command -v")
                              ("free" . "free -h")
                              ("mime" . "file -b --mime-type")))
                   (bashrc
                    (list (plain-file "rc"
                                      (string-append
                                       "\n"
                                       "[ -f /etc/bashrc ] && source /etc/bashrc"
                                       "\n\n"
                                       "shopt -s histappend\n"
                                       "HISTCONTROL=ignoreboth:erasedups\n"
                                       "HISTFILESIZE=10000\n"
                                       "HISTSIZE=$HISTFILESIZE\n"
                                       "HISTFILE=$XDG_CACHE_HOME/bash_history"
                                       "\n\n"
                                       "if [ -n \"$GUIX_ENVIRONMENT\" ]; then\n"
                                       "    PS1=\"\\w (env) λ \"\n"
                                       "else\n"
                                       "    PS1=\"\\w λ \"\n"
                                       "fi"
                                       "\n\n"
                                       "if [ -n \"$EAT_SHELL_INTEGRATION_DIR\" ]; then\n"
                                       "    source \"$EAT_SHELL_INTEGRATION_DIR/bash\"\n"
                                       "fi"
                                       "\n\n"
                                       "command -v direnv >/dev/null && eval \"$(direnv hook bash)\""))))))
         (service home-dbus-service-type)
         (service home-gpg-agent-service-type
                  (home-gpg-agent-configuration
                   (gnupg-home
                    (string-append (getenv "HOME") "/.local/share/gnupg"))
                   (pinentry-program
                    (file-append pinentry-emacs "/bin/pinentry-emacs"))
                   (extra-content (format #f "~@{~a~%~}"
                                          "allow-emacs-pinentry"
                                          "allow-loopback-pinentry"))))
         (service home-inputrc-service-type
                  (home-inputrc-configuration
                   (variables `(("show-all-if-ambiguous" . #t)
                                ("completion-ignore-case" . #t)
                                ("colored-stats" . #t)
                                ("mark-symlinked-directories" . #t)
                                ("editing-mode" . "emacs")
                                ("show-mode-in-prompt" . #t)
                                ("emacs-mode-string" . "\\1\\e[6 q\\2")))))
         (service home-pipewire-service-type)
         (service home-xdg-user-directories-service-type
                  (home-xdg-user-directories-configuration
                   (desktop "$HOME/desktop")
                   (documents "$HOME/documents")
                   (download "$HOME/downloads")
                   (music "$HOME/musics")
                   (pictures "$HOME/pictures")
                   (publicshare "$HOME/public")
                   (templates "$HOME/templates")
                   (videos "$HOME/videos")))
         (service home-batsignal-service-type
                  (home-batsignal-configuration
                   (warning-level 20)
                   (critical-level 5)
                   (danger-level 2)
                   (danger-command "loginctl hibernate")
                   (full-level 90)))
         %base-home-services)))
