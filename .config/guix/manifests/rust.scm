(use-modules
 (gnu packages llvm)
 (rustup build toolchain))

(packages->manifest
 (list clang-toolchain
       (rustup #:components
               '(cargo clippy rustc rust-src rust-std rust-docs rust-analyzer))))
