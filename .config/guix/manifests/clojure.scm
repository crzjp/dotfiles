(use-modules
 (gnu packages java)
 (nongnu packages clojure))

(setenv "LEIN_HOME" (string-append (getenv "HOME") "/.local/share/lein"))

(packages->manifest
 (list leiningen
       (list openjdk "jdk")))
